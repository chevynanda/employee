import 'package:flutter/material.dart';

class TextFieldCustom {
  static textFieldGeneral(String label,
      {bool? isDisabled, String? prefixText}) {
    return InputDecoration(
      contentPadding: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
      labelText: label,
      prefixText: prefixText,
      labelStyle: isDisabled != null
          ? isDisabled
              ? const TextStyle(color: Colors.grey, fontStyle: FontStyle.italic)
              : const TextStyle(color: Colors.black)
          : const TextStyle(color: Colors.black),
      hintStyle: const TextStyle(color: Colors.black),
      fillColor: Colors.red,
      focusedBorder: const OutlineInputBorder(
        borderSide: BorderSide(color: Colors.grey, width: 1),
      ),
      disabledBorder: const OutlineInputBorder(
        borderSide: BorderSide(color: Colors.grey, width: 1),
      ),
      enabledBorder: const OutlineInputBorder(
        borderSide: BorderSide(color: Colors.grey, width: 1),
      ),
      errorBorder: const OutlineInputBorder(
        borderSide: BorderSide(color: Colors.red, width: 1),
      ),
    );
  }

  static textFieldKomentar(String label, Function press) {
    OutlineInputBorder outlineinputborder = OutlineInputBorder(
      borderRadius: BorderRadius.circular(5),
      borderSide: const BorderSide(color: Colors.grey, width: 1),
    );
    return InputDecoration(
      isCollapsed: true,
      contentPadding: const EdgeInsets.all(15),
      hintText: label,
      hintStyle: const TextStyle(color: Colors.grey),
      fillColor: Colors.red,
      focusedBorder: outlineinputborder,
      disabledBorder: outlineinputborder,
      enabledBorder: outlineinputborder,
      errorBorder: outlineinputborder,
      suffixIcon: IconButton(
        icon: const Icon(Icons.send, color: Colors.grey),
        onPressed: () {
          press;
        },
      ),
    );
  }

  static textFieldMultiple(String label, Function press) {
    return InputDecoration(
      labelText: label,
      labelStyle: const TextStyle(color: Colors.black),
      hintStyle: const TextStyle(color: Colors.black),
      fillColor: Colors.red,
      focusedBorder: const OutlineInputBorder(
        borderSide: BorderSide(color: Colors.orange, width: 1),
      ),
      disabledBorder: const OutlineInputBorder(
        borderSide: BorderSide(color: Colors.grey, width: 1),
      ),
      enabledBorder: const OutlineInputBorder(
        borderSide: BorderSide(color: Colors.grey, width: 1),
      ),
      errorBorder: const OutlineInputBorder(
        borderSide: BorderSide(color: Colors.red, width: 1),
      ),
      suffixIcon: IconButton(
        icon: const Icon(Icons.close),
        onPressed: () {
          press;
        },
      ),
    );
  }

  static textFieldComment(String label, Function onTap) {
    return InputDecoration(
      hintText: label,
      labelStyle: const TextStyle(color: Colors.black),
      hintStyle: const TextStyle(color: Colors.grey),
      //fillColor: Colors.red,
      focusedBorder: const OutlineInputBorder(
        borderRadius: BorderRadius.all(
          Radius.circular(100),
        ),
        borderSide: BorderSide(color: Colors.grey, width: 1),
      ),
      disabledBorder: const OutlineInputBorder(
        borderRadius: BorderRadius.all(
          Radius.circular(100),
        ),
        borderSide: BorderSide(color: Colors.grey, width: 1),
      ),
      enabledBorder: const OutlineInputBorder(
        borderRadius: BorderRadius.all(
          Radius.circular(100),
        ),
        borderSide: BorderSide(color: Colors.grey, width: 1),
      ),
      errorBorder: const OutlineInputBorder(
        borderRadius: BorderRadius.all(
          Radius.circular(100),
        ),
        borderSide: BorderSide(color: Colors.red, width: 1),
      ),
      suffix: InkWell(
        onTap: () {
          onTap;
        },
        child: const Text(
          "",
          style: TextStyle(color: Colors.orange),
        ),
      ),
    );
  }

  static textfieldSelect(String label) {
    return InputDecoration(
        labelStyle: const TextStyle(color: Colors.black, fontSize: 16.0),
        errorStyle: const TextStyle(color: Colors.redAccent, fontSize: 16.0),
        hintText: label,
        labelText: label,
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(5.0)));
  }

  static textFieldDate(String label) {
    return InputDecoration(
      labelText: label,
      //hintText: "Masukan Keyword",
      labelStyle: const TextStyle(color: Colors.black),
      hintStyle: const TextStyle(color: Colors.black),
      fillColor: Colors.red,
      focusedBorder: const OutlineInputBorder(
        borderSide: BorderSide(color: Colors.orange, width: 1),
      ),
      enabledBorder: const OutlineInputBorder(
        borderSide: BorderSide(color: Colors.grey, width: 1),
      ),
      suffixIcon: IconButton(
          icon: const Icon(
            Icons.date_range,
            color: Colors.orange,
          ),
          onPressed: () {}),

      //fillColor: Colors.green
    );
  }

  static textFieldAttachment(String label) {
    return InputDecoration(
      labelText: label,
      hintText: label,
      labelStyle: const TextStyle(color: Colors.black),
      hintStyle: const TextStyle(color: Colors.black),
      fillColor: Colors.red,
      focusedBorder: const OutlineInputBorder(
        borderSide: BorderSide(color: Colors.orange, width: 1),
      ),
      enabledBorder: const OutlineInputBorder(
        borderSide: BorderSide(color: Colors.grey, width: 1),
      ),
      suffixIcon: IconButton(
          icon: const Icon(
            Icons.attach_file,
            color: Colors.orange,
          ),
          onPressed: () {}),

      //fillColor: Colors.green
    );
  }

  static textFiledLogin(String label, IconData icon, [IconButton? suffixIcon]) {
    return InputDecoration(
      hintText: label,
      labelText: label,
      labelStyle: const TextStyle(color: Colors.grey),
      hintStyle: const TextStyle(color: Colors.grey),
      fillColor: Colors.white,
      prefixIcon: Icon(
        icon,
        color: Colors.grey,
      ),
      suffixIcon: suffixIcon,
      border: InputBorder.none,
      filled: true,
      focusedBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: Colors.grey),
          borderRadius: BorderRadius.circular(5),
          gapPadding: 5),
      enabledBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: Colors.grey),
          borderRadius: BorderRadius.circular(5),
          gapPadding: 5),
    );
  }

  static textFieldSearch({Function? onPressed}) {
    return InputDecoration(
      labelText: "Search",
      labelStyle: const TextStyle(color: Colors.black),
      hintStyle: const TextStyle(color: Colors.black),
      fillColor: Colors.red,
      focusedBorder: const OutlineInputBorder(
        borderSide: BorderSide(color: Colors.orange, width: 1),
      ),
      enabledBorder: const OutlineInputBorder(
        borderSide: BorderSide(color: Colors.grey, width: 1),
      ),
      suffixIcon: IconButton(
          icon: const Icon(
            Icons.search,
            color: Colors.orange,
          ),
          onPressed: () {
            onPressed;
          }),
      //fillColor: Colors.green
    );
  }

  static textFieldSearch2({Function? onPressed}) {
    return InputDecoration(
      prefixIcon: IconButton(
        icon: const Icon(
          Icons.search,
          color: Color(0XFF9D9D9D),
        ),
        onPressed: () {},
      ),
      fillColor: Colors.white,
      filled: true,
      hintText: "Search",
      focusedBorder: const OutlineInputBorder(
        borderSide: BorderSide(color: Colors.orange, width: 1),
      ),
      enabledBorder: const OutlineInputBorder(
        borderSide: BorderSide(color: Colors.grey, width: 1),
      ),
    );
  }
}
