import 'package:flutter/material.dart';

class PrimaryLoading extends StatelessWidget {
  const PrimaryLoading({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: CircularProgressIndicator(color: Colors.deepPurple),
    );
  }
}
