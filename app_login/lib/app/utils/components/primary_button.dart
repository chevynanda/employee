import 'package:flutter/material.dart';

class PrimaryButton extends StatelessWidget {
  final String? label;
  final Function()? onPress;

  const PrimaryButton({Key? key, @required this.label, this.onPress})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return TextButton(
      style: TextButton.styleFrom(
        foregroundColor: Colors.white,
        backgroundColor: Colors.blueAccent,
        padding: const EdgeInsets.all(18.0),
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(5.0)),
        ),
      ),
      onPressed: onPress,
      child: Text(
        label ?? '',
        style: const TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
      ),
    );
  }
}
