class BaseResponseApi {
  BaseResponseApi({this.status, this.message, required this.data});

  factory BaseResponseApi.fromJson(Map<String, dynamic> json) =>
      BaseResponseApi(
        data: json['data'] as List<dynamic>?,
      );

  bool? status;
  String? message;

  List<dynamic>? data;

  Map<String, dynamic> toJson() => {
        'data': data,
      };
}
