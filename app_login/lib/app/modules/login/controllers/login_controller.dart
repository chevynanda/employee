import 'package:app_login/app/routes/app_pages.dart';

import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../data/shared_preference/shared_preference.dart';
import '../models/login_model.dart';
import '../repository/login_repository.dart';
import '../repository/login_service.dart';

class LoginController extends GetxController {
  Rx<LoginModel> loginData = LoginModel().obs;
  final LoginRepository loginRepo = LoginServices();
  List<String>? suggestionList = ["gmail.com"];
  TextEditingController passwordCont = TextEditingController();
  TextEditingController username = TextEditingController();

  final isHide = true.obs;
  int a = 0;
  @override
  void onInit() {
    cekToken();
    username = TextEditingController();
    passwordCont = TextEditingController();
    super.onInit();
  }

  cekToken() async {
    String? cektoken = await PreferenceHandler.retrieveToken();
    if (cektoken!.isNotEmpty) {
      Get.offAllNamed(Routes.HOME);
    }
  }

  void togglepassword() {
    a++;
    if (a == 1) {
      update();
      isHide.value = false;
    }
    if (a == 2) {
      update();
      isHide.value = true;
      a = 0;
    }
  }

  postLogin(String username, String password) async {
    update();

    final res = await loginRepo.getData(username: username, password: password);
    try {
      update();

      // ignore: unnecessary_null_comparison
      if (res == null) {
        loginData.value;
        Get.snackbar(
          "Perhatian!",
          "Gagal login",
          snackPosition: SnackPosition.BOTTOM,
          colorText: Colors.white,
          backgroundColor: Colors.red,
          icon: const Icon(Icons.warning),
        );
        update();
      } else {
        update();
        debugPrint("hasil debug");

        PreferenceHandler.storingToken(res.token.toString());
        String? token = await PreferenceHandler.retrieveToken();
        debugPrint(token);
        Get.snackbar(
          "Sukses",
          "Berhasil login",
          snackPosition: SnackPosition.BOTTOM,
          colorText: Colors.white,
          backgroundColor: Colors.green,
          icon: const Icon(
            Icons.check,
            color: Colors.white,
          ),
        );

        loginData.value = res;
        Get.offAllNamed(Routes.HOME);
      }

      update();
    } catch (e) {
      Get.snackbar(
        "Perhatian!",
        "Gagal login",
        snackPosition: SnackPosition.BOTTOM,
        colorText: Colors.white,
        backgroundColor: Colors.red,
        icon: const Icon(Icons.warning),
      );
      update();
    }
  }
}
