import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../../utils/components/costume_textfield.dart';
import '../../../utils/components/primary_button.dart';
import '../../../utils/dimens.dart';
import '../controllers/login_controller.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:crypto/crypto.dart';
import 'dart:convert';

class LoginView extends GetView<LoginController> {
  const LoginView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
      child: SizedBox(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Icon(Icons.people),
            Container(
              padding: const EdgeInsets.only(
                  top: 51,
                  left: MyDimens.marginActivity,
                  right: MyDimens.marginActivity),
              child: TypeAheadFormField(
                textFieldConfiguration: TextFieldConfiguration(
                    controller: controller.username,
                    style: const TextStyle(color: Colors.grey),
                    textAlign: TextAlign.left,
                    decoration: TextFieldCustom.textFiledLogin(
                        "Username", Icons.person)),
                suggestionsCallback: (pattern) async {
                  return controller.suggestionList!;
                },
                itemBuilder: (context, suggestion) {
                  return ListTile(
                    title: Text(suggestion.toString()),
                  );
                },
                transitionBuilder: (context, suggestionsBox, controller) {
                  return suggestionsBox;
                },
                onSuggestionSelected: (suggestion) {
                  controller.username.text =
                      controller.username.text + suggestion;
                },
              ),
            ),
            Container(
                padding: const EdgeInsets.only(
                    left: MyDimens.marginActivity,
                    right: MyDimens.marginActivity,
                    top: MyDimens.marginActivity),
                child: Column(children: <Widget>[
                  Obx(
                    () => TextField(
                      style: const TextStyle(color: Colors.grey),
                      controller: controller.passwordCont,
                      textAlign: TextAlign.left,
                      obscureText: controller.isHide.value,
                      decoration: TextFieldCustom.textFiledLogin(
                        "Password",
                        Icons.lock,
                        IconButton(
                          icon: controller.isHide.isTrue
                              ? Icon(
                                  Icons.visibility_off,
                                  color: controller.isHide.isTrue
                                      ? Colors.grey
                                      : Colors.grey,
                                )
                              : const Icon(
                                  Icons.visibility,
                                ),
                          onPressed: () {
                            controller.togglepassword();
                          },
                        ),
                      ),
                    ),
                  )
                ])),
            const SizedBox(
              height: 10.0,
            ),
            Container(
              margin: const EdgeInsets.all(MyDimens.marginActivity),
              width: MediaQuery.of(context).size.width,
              child: PrimaryButton(
                label: "Login",
                onPress: () {
                  if (controller.username.text.isEmpty ||
                      controller.passwordCont.text.isEmpty) {
                    Get.snackbar("Perhatian", "Isian tidak boleh kosong");
                  } else {
                    // then hash the string
                    var bytes = utf8.encode(
                        controller.passwordCont.text); // data being hashed
                    var digest = sha256.convert(bytes);
                    debugPrint("$digest");
                    controller.update();
                    controller.postLogin(
                        controller.username.text, digest.toString());
                  }
                },
              ),
            ),
          ],
        ),
      ),
    ));
  }
}
