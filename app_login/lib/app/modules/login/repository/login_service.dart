import 'dart:convert';

import 'package:app_login/app/modules/login/models/login_model.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart' hide FormData, Response;

import '../../../data/networks/base_response.dart';
import '../../../data/networks/network_dio.dart';
import 'login_repository.dart';

class LoginServices extends LoginRepository {
  Dio get _dio => NetworkDio.createDio(needAuth: true);
  @override
  Future<LoginModel> getData({String? username, String? password}) async {
    FormData formData = FormData.fromMap({
      "user_id": username,
      "user_password": password,
    });
    try {
      Response response = await _dio.post('/login', data: formData);
      print("data ${jsonDecode(response.data)}");
      return LoginModel.fromJson(jsonDecode(response.data));
    } on DioError catch (e) {
      BaseResponseApi error =
          BaseResponseApi.fromJson(jsonDecode(e.response!.data));
      Get.snackbar(
        "Perhatian!",
        "Kombinasi user dan password tidak ditemukan",
        snackPosition: SnackPosition.BOTTOM,
        colorText: Colors.white,
        backgroundColor: Colors.red,
        icon: const Icon(Icons.warning),
      );
      throw '$error';
    }
  }
}
