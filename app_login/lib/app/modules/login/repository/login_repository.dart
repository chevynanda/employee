import '../models/login_model.dart';

abstract class LoginRepository {
  Future<LoginModel> getData(
      {required String username, required String password});
}
