import 'package:app_login/app/modules/home/models/employee_model.dart';
import 'package:app_login/app/modules/home/repository/services.dart';
import 'package:get/get.dart';

class HomeController extends GetxController {
  RxList<EmployeeModel> dataList = <EmployeeModel>[].obs;
  final EmployeeRepository dataRepo = EmployeeServices();
  var isLoading = false.obs;

  @override
  void onInit() {
    getload();
    super.onInit();
  }

  getload() async {
    update();

    final res = await dataRepo.getData();
    try {
      isLoading.value = false;
      update();

      if (res!.isEmpty) {
        isLoading.value = false;
        dataList.value = [];
        update();
      } else {
        update();
        isLoading.value = true;
        dataList.value = res;
      }
      isLoading.value = false;
      update();
    } catch (e) {
      Get.snackbar("Error", "Data tidak di temukan");
      isLoading.value = false;
      update();
    }
  }
}
