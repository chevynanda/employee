class EmployeeModel {
  int? employeeId;
  String? employeeName;
  String? employeeAddress;
  String? employeeDob;
  String? employeeMail;
  int? departmentId;
  int? flagEmployee;
  String? branchId;
  String? branchName;
  String? departmentName;

  EmployeeModel(
      {this.employeeId,
      this.employeeName,
      this.employeeAddress,
      this.employeeDob,
      this.employeeMail,
      this.departmentId,
      this.flagEmployee,
      this.branchId,
      this.branchName,
      this.departmentName});

  EmployeeModel.fromJson(Map<String, dynamic> json) {
    employeeId = json['employee_id'];
    employeeName = json['employee_name'];
    employeeAddress = json['employee_address'];
    employeeDob = json['employee_dob'];
    employeeMail = json['employee_mail'];
    departmentId = json['department_id'];
    flagEmployee = json['flag_employee'];
    branchId = json['branch_id'];
    branchName = json['branch_name'];
    departmentName = json['department_name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['employee_id'] = employeeId;
    data['employee_name'] = employeeName;
    data['employee_address'] = employeeAddress;
    data['employee_dob'] = employeeDob;
    data['employee_mail'] = employeeMail;
    data['department_id'] = departmentId;
    data['flag_employee'] = flagEmployee;
    data['branch_id'] = branchId;
    data['branch_name'] = branchName;
    data['department_name'] = departmentName;
    return data;
  }
}
