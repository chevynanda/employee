import 'package:app_login/app/data/shared_preference/shared_preference.dart';
import 'package:app_login/app/routes/app_pages.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../../utils/components/primary_loading.dart';
import '../controllers/home_controller.dart';

class HomeView extends GetView<HomeController> {
  const HomeView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          backgroundColor: Colors.indigo[800],
          title: const Text('List Employee'),
          centerTitle: true,
          actions: [
            IconButton(
                onPressed: () {
                  PreferenceHandler.removeToken();
                  Get.toNamed(Routes.LOGIN);
                },
                icon: const Icon(Icons.logout))
          ],
        ),
        body: Obx(() => controller.dataList.isNotEmpty
            ? SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: ListView.builder(
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    itemCount: controller.dataList.length,
                    itemBuilder: (context, index) {
                      return Card(
                        clipBehavior: Clip.antiAliasWithSaveLayer,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        elevation: 5,
                        child: ListTile(
                          title: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  const SizedBox(
                                    height: 6,
                                  ),
                                  _buildRow("ID",
                                      "${controller.dataList[index].employeeId ?? "-"}"),
                                  const SizedBox(
                                    height: 10,
                                  ),
                                  _buildRow(
                                      "Nama",
                                      controller.dataList[index].employeeName ??
                                          "-"),
                                  const SizedBox(
                                    height: 10,
                                  ),
                                  _buildRow(
                                      "Email",
                                      controller.dataList[index].employeeMail ??
                                          "-"),
                                  const SizedBox(
                                    height: 10,
                                  ),
                                  const SizedBox(
                                    height: 10,
                                  ),
                                ]),
                          ),
                          onTap: () {},
                        ),
                      );
                    }))
            : const Center(
                child: PrimaryLoading(),
              )));
  }

  Widget _buildRow(String firstRow, String secondRow) => Row(
        children: <Widget>[
          Expanded(
              flex: 2,
              child: Text(firstRow,
                  style: const TextStyle(fontWeight: FontWeight.bold))),
          Expanded(flex: 3, child: Text(secondRow)),
        ],
      );
}
