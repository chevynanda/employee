import 'dart:convert';

import 'package:app_login/app/modules/home/models/employee_model.dart';
import 'package:app_login/app/modules/home/repository/services.dart';
import 'package:dio/dio.dart';

import '../../../data/networks/base_response.dart';
import '../../../data/networks/network_dio.dart';
import '../../../data/shared_preference/shared_preference.dart';

class EmployeeServices extends EmployeeRepository {
  Dio get _dio => NetworkDio.createDio(needAuth: true);

  @override
  Future<List<EmployeeModel>?> getData() async {
    try {
      String? token = await PreferenceHandler.retrieveToken();
      Response response = await _dio.get(
        '/employee/list?branch_id=%&department_id=%',
        options: Options(
          headers: {"token": "$token"},
        ),
      );

      final resp = BaseResponseApi.fromJson(
          jsonDecode(response.toString()) as Map<String, dynamic>);

      if (resp != null) {
        final getData = (resp.data as List)
            .map((e) => EmployeeModel.fromJson(e as Map<String, dynamic>))
            .toList();

        return getData;
      } else {
        return [];
      }
    } on DioError catch (e) {
      BaseResponseApi error =
          BaseResponseApi.fromJson(jsonDecode(e.response!.data));
      throw '$error';
    }
  }
}
