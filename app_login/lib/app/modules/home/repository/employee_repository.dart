import 'package:app_login/app/modules/home/models/employee_model.dart';

abstract class EmployeeRepository {
  Future<List<EmployeeModel>?> getData();
}
